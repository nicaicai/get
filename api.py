#! /usr/bin/env python
# _*_ coding: utf-8 _*_
from flask import *
from flask_sqlalchemy import SQLAlchemy
import requests
import io
import sys
sys.stdout = io.TextIOWrapper(sys.stdout.buffer,encoding='utf-8')
from diffjson import diffJson

app = Flask(__name__)
app.secret_key = 'min'

@app.route('/',methods=['GET'])
def hello():
	return 'Hello Min!'

@app.route('/test_json_one',methods=['GET'])
def test_json_one():
	res = {'code':200,'msg':'success','data':'test_json_one'}
	return jsonify(res)

@app.route('/test_json_two',methods=['GET'])
def test_json_two():
	res = {'code':200,'msg':'success','data':'test_json_two'}
	return jsonify(res)

# @app.route('/diff_json',methods=['GET'])
# def diff_json():
# 	res = {'code':200,'msg':'success','data':'info'}
# 	return jsonify(res)

@app.route('/get_request',methods=['GET','POST'])
def get_request():
	return render_template('get.html')

@app.route('/do_get',methods=['GET','POST'])
def doGet():
	params = request.json
	urlOne = params['urlOne']
	urlTwo = params['urlTwo']
	# print(urlOne)
	# print(urlTwo)
	res = {}
	responseOne = -1
	responseTwo = -1


	try:
		responseOne = requests.get(url=urlOne,verify=False).json()
		# print(responseOne)
		res['responseOne'] = responseOne
	except Exception as e:
		# raise e
		# print(urlOne)
		# print(responseOne)
		responseOne = -1
		res['responseOne'] = {"msg":"请求1无效"}
	finally:
		try:
			responseTwo = requests.get(url=urlTwo,verify=False).json()
			# print(responseTwo)
			res['responseTwo'] = responseTwo
		except Exception as e:
			# raise e
			responseTwo = -1
			res['responseTwo'] = {"msg":"请求2无效"}
		res['diff'] = diffJson(responseOne,responseTwo)
		return jsonify(res)
		pass
		
# @app.route('/demo',methods=['GET','POST'])
# def json_demo():
# 	return render_template('demo.html')

if __name__ == '__main__':
	app.run(debug=True, host='127.0.0.1', port=3391)
