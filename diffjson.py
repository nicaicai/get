import json_tools
import json

def diffJson(dic1,dic2):
    # print(dicOne)
    # print(dicTwo)
    # print(type(dicOne))
    # print(dicTwo)
    # dic1 = json.loads(dicOne)
    # dic2 = json.loads(dicTwo)

    diff = json_tools.diff(dic1,dic2)
    # print(8888)
    # print(diff)
    # print(len(diff))
    if len(diff)==0:
        res = {"json-diff":"两段json一致"}
        return res
    else:
        # print(diff)
        # location = diff[0]['replace']
        # print(location)
        # c = location.split('/')
        # c.pop(0)
        # print(c)

        # i = dic1
        # for x in c:
        #     i = i[x]
        # # 获取不一样的key和value
        # dicx = {}
        # dicx[c[-1]] = i
        # print(dicx)

        diff = del_replace(diff)
        return diff

def del_replace(diff):
    '''
    去除replace部分
    :param diff:
    :return:
    '''
    res = []
    for action in diff:
        if 'add' in action or 'remove' in action:
            res.append(action)

    return res

if __name__ == '__main__':
    # dic1 = {
    #   "a":"1",
    #   "b":"2",
    #   "z":"1",
    #   "c":"3"
    # }

    # dic2={
    #   "a":"1",
    #   "z":"10",
    #   "b":"2",
    #   "c":"3"
    # }
    dic1 = '{"a":"1","b":"2","z":{"a":"1"},"c":"3"}'
    dic2 = '{"a":"1","z":{"a":"10"},"b":"2","c":"3"}'

    dic3 = {
		"error_msg": "",
		"error_no": 0,
		"result": {
			"find_liked_list": [{
				"activity_msg": "\u6ee135\u51cf12",
				"current_price": "32.0",
				"dish_id": "159817828275",
				"dish_name": "\u9999\u693f\u82d7\u7092\u9e21\u86cb",
				"dish_pic": "https://fuss10.elemecdn.com/9/8b/52ee0c0fab6004974327183c84aa2jpeg.jpeg",
				"dish_price": "32.0",
				"shop_id": "1879677567",
				"shop_name": "\u84c9\u84c9\u9601\u70e4\u9e2d(\u5706\u660e\u56ed\u5e97)",
				"shopdish_url": "bdwm://native?pageName=shopMenu&shopId=1879677567&categoryId=26340628403&dishId=159817828275"
			}, {
				"activity_msg": "\u6ee122\u51cf10",
				"current_price": "36.8",
				"dish_id": "100000037791194517",
				"dish_name": "\u6e2f\u5f0f\u5496\u55b1\u9e21\u5b9a\u98df",
				"dish_pic": "https://fuss10.elemecdn.com/3/ff/877c7dd9472333f7a2595adf5a99djpeg.jpeg",
				"dish_price": "36.8",
				"shop_id": "2072015245",
				"shop_name": "\u81b3\u5320(\u7b2c8\u53f7\u6863\u53e3\u65b0\u65f6\u4ee3\u7f8e\u98df\u57ce\u5e97)",
				"shopdish_url": "bdwm://native?pageName=shopMenu&shopId=2072015245&categoryId=41292695957&dishId=100000037791194517"
			}, {
				"activity_msg": "\u6ee135\u51cf4",
				"current_price": "34.0",
				"dish_id": "328944076379",
				"dish_name": "\u5fb7\u5f0f\u718f\u80a0\u6c99\u62c9",
				"dish_pic": "https://fuss10.elemecdn.com/5/1d/4861149afa1175fb20229fff27be2jpeg.jpeg",
				"dish_price": "34.0",
				"shop_id": "2148127526",
				"shop_name": "Hey Fresh\u6c99\u62c9\u4e0e\u70ed\u72d7(\u4e2d\u5173\u6751\u8f6f\u4ef6\u56ed\u5e97)",
				"shopdish_url": "bdwm://native?pageName=shopMenu&shopId=2148127526&categoryId=45674653275&dishId=328944076379"
			}, {
				"activity_msg": "\u6ee140\u51cf22",
				"current_price": "35.9",
				"dish_id": "496675140382",
				"dish_name": "\u660e\u661f\u5408\u5229\u5c4b\u9e21\u8089\u5957\u9910",
				"dish_pic": "https://fuss10.elemecdn.com/1/7f/c06e5bf45367193a5fd1e9678401ejpeg.jpeg",
				"dish_price": "60.0",
				"shop_id": "164593916",
				"shop_name": "\u5408\u5229\u5c4b(\u767e\u65fa\u5e97)",
				"shopdish_url": "bdwm://native?pageName=shopMenu&shopId=164593916&categoryId=100000006332489502&dishId=496675140382"
			}, {
				"activity_msg": "\u6ee155\u51cf5",
				"current_price": "28.0",
				"dish_id": "350891510318",
				"dish_name": "\u897f\u57df\u79d8\u5236\u6912\u9ebb\u9e21(\u5355\u4eba\u4efd)(\u5355\u4eba\u4efd)",
				"dish_pic": "https://fuss10.elemecdn.com/3/0a/c876fd90b2b2cf3afd1c4657c9cb7jpeg.jpeg",
				"dish_price": "28.0",
				"shop_id": "2179625595",
				"shop_name": "\u897f\u7c89\u5802\u65b0\u7586\u7c73\u7c89(\u4e0a\u5730\u5e97)",
				"shopdish_url": "bdwm://native?pageName=shopMenu&shopId=2179625595&categoryId=47004863022&dishId=350891510318"
			}, {
				"activity_msg": None,
                "good":"test1",
				"current_price": "25.0",
				"dish_id": "518446878356",
				"dish_name": "\u867e\u4ec1\u4e09\u9c9c\u6c34\u997a/\u76d8",
				"dish_pic": "https://fuss10.elemecdn.com/8/9c/bf3f7e00c60519de6c5ebf1ecff7ejpeg.jpeg",
				"dish_price": "25.0",
				"shop_id": "156682761",
				"shop_name": "\u5e86\u4e30\u5305\u5b50\u94fa\uff08\u901a\u53a6\u5e97\uff09",
				"shopdish_url": "bdwm://native?pageName=shopMenu&shopId=156682761&categoryId=58618858132&dishId=518446878356"
			}],
			"likedNum": 6,
            "fanalTest":[]
		}
	}

    dic4 = {
		"error_msg": "",
		"error_no": 0,
		"result": {
			"find_liked_list": [{
				"activity_msg": "\u6ee140\u51cf22",
				"current_price": "35.9",
				"dish_id": "496675140382",
				"dish_name": "\u660e\u661f\u5408\u5229\u5c4b\u9e21\u8089\u5957\u9910",
				"dish_pic": "https://fuss10.elemecdn.com/1/7f/c06e5bf45367193a5fd1e9678401ejpeg.jpeg",
				"dish_price": "60.0",
				"shop_id": "164593916",
				"shop_name": "\u5408\u5229\u5c4b(\u767e\u65fa\u5e97)",
				"shopdish_url": "bdwm://native?pageName=shopMenu&shopId=164593916&categoryId=100000006332489502&dishId=496675140382"
			}, {
				"activity_msg": "\u6ee122\u51cf10",
				"current_price": "36.8",
				"dish_id": "100000037791194517",
				"dish_name": "\u6e2f\u5f0f\u5496\u55b1\u9e21\u5b9a\u98df",
				"dish_pic": "https://fuss10.elemecdn.com/3/ff/877c7dd9472333f7a2595adf5a99djpeg.jpeg",
				"dish_price": "36.8",
				"shop_id": "2072015245",
				"shop_name": "\u81b3\u5320(\u7b2c8\u53f7\u6863\u53e3\u65b0\u65f6\u4ee3\u7f8e\u98df\u57ce\u5e97)",
				"shopdish_url": "bdwm://native?pageName=shopMenu&shopId=2072015245&categoryId=41292695957&dishId=100000037791194517"
			}, {
				"activity_msg": "\u6ee130\u51cf9",
				"current_price": "25.0",
				"dish_id": "464653986426",
				"dish_name": "\u97ed\u83dc\u76d2\u5b50",
				"dish_pic": "https://fuss10.elemecdn.com/d/91/ddce2d657194ad50642cf948cab97jpeg.jpeg",
				"dish_price": "25.0",
				"shop_id": "162609161",
				"shop_name": "\u4e1c\u5317\u83dc\u9986(\u4e94\u5f69\u57ce\u5e97)",
				"shopdish_url": "bdwm://native?pageName=shopMenu&shopId=162609161&categoryId=54792705658&dishId=464653986426"
			}, {
				"activity_msg": "\u6ee180\u51cf22",
				"current_price": "29",
				"dish_id": "281877396076",
				"dish_name": "\u9521\u76df\u80a5\u725b\u8089",
				"dish_pic": "https://fuss10.elemecdn.com/1/c8/4991755c78a7e8524ef0330420c6djpeg.jpeg",
				"dish_price": "58.0",
				"shop_id": "1540153997",
				"shop_name": "\u874e\u738b\u5e9c\u7f8a\u874e\u5b50(\u6e05\u6cb3\u5e97)",
				"shopdish_url": "bdwm://native?pageName=shopMenu&shopId=1540153997&categoryId=41540990572&dishId=281877396076"
			}, {
				"activity_msg": "\u6ee155\u51cf5",
				"current_price": "28.0",
				"dish_id": "350891510318",
				"dish_name": "\u897f\u57df\u79d8\u5236\u6912\u9ebb\u9e21(\u5355\u4eba\u4efd)(\u5355\u4eba\u4efd)",
				"dish_pic": "https://fuss10.elemecdn.com/3/0a/c876fd90b2b2cf3afd1c4657c9cb7jpeg.jpeg",
				"dish_price": "28.0",
				"shop_id": "2179625595",
				"shop_name": "\u897f\u7c89\u5802\u65b0\u7586\u7c73\u7c89(\u4e0a\u5730\u5e97)",
				"shopdish_url": "bdwm://native?pageName=shopMenu&shopId=2179625595&categoryId=47004863022&dishId=350891510318"
			}, {
				"activity_msg": None,
                "something":"test",
				"current_price": "25.0",
				"dish_id": "518446878356",
				"dish_name": "\u867e\u4ec1\u4e09\u9c9c\u6c34\u997a/\u76d8",
				"dish_pic": "https://fuss10.elemecdn.com/8/9c/bf3f7e00c60519de6c5ebf1ecff7ejpeg.jpeg",
				"dish_price": "25.0",
				"shop_id": "156682761",
				"shop_name": "\u5e86\u4e30\u5305\u5b50\u94fa\uff08\u901a\u53a6\u5e97\uff09",
				"shopdish_url": "bdwm://native?pageName=shopMenu&shopId=156682761&categoryId=58618858132&dishId=518446878356"
			}],
			"likedNum": 7,
            "fanalTest": [{
                "abc":123
            }]
		}
	}

    print(diffJson(dic3,dic4))